﻿USE disparadores1;

# DISPARADOR
-- Crear un disparador para que cuando INSERTE un empresa calcule 
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada

DROP TRIGGER IF EXISTS biempresas;
DELIMITER //
CREATE TRIGGER biempresas
BEFORE INSERT
  ON empresas
  FOR EACH ROW
BEGIN
  SET @@lc_time_names='es_es'; -- LOS MESES Y DIAS EN CASTELLANO
  SET 
    NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
    NEW.diaMes=DAY(NEW.fechaEntrada),
    NEW.mes=MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero=MONTH(NEW.fechaEntrada),
    NEW.anno=year(NEW.fechaEntrada);
END //
DELIMITER ;



-- INSERTAR UNA EMPRESA

INSERT INTO empresas (empresa, fechaEntrada)
  VALUES ('juguetes norma', '2021/12/4');

-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;

# DISPARADOR
-- Crear un disparador para que cuando ACTUALIZAR una empresa me coloque correctamente
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada

DROP TRIGGER IF EXISTS buempresas;
DELIMITER //
CREATE TRIGGER buempresas
BEFORE UPDATE
  ON empresas
  FOR EACH ROW
BEGIN
  SET @@lc_time_names='es_es'; -- LOS MESES Y DIAS EN CASTELLANO
  SET 
    NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
    NEW.diaMes=DAY(NEW.fechaEntrada),
    NEW.mes=MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero=MONTH(NEW.fechaEntrada),
    NEW.anno=year(NEW.fechaEntrada);
END //
DELIMITER ;

-- ACTUALIZO UNA EMPRESA

UPDATE empresas e
  SET e.fechaEntrada='2021/8/14'
  WHERE e.empresa='alpe';


-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;


-- Modificar el disparador anterior para que en caso de que 
-- al actualizar la empresa no se modidique la fecha de entrada
-- no realice ningun calculo

DROP TRIGGER IF EXISTS buempresas;
DELIMITER //
CREATE TRIGGER buempresas
BEFORE UPDATE
  ON empresas
  FOR EACH ROW
BEGIN
  SET @@lc_time_names='es_es'; -- LOS MESES Y DIAS EN CASTELLANO

  IF (NEW.fechaEntrada<>OLD.fechaEntrada) THEN
    SET 
      NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
      NEW.diaMes=DAY(NEW.fechaEntrada),
      NEW.mes=MONTHNAME(NEW.fechaEntrada),
      NEW.mesNumero=MONTH(NEW.fechaEntrada),
      NEW.anno=year(NEW.fechaEntrada);
  END IF;
  
END //
DELIMITER ;



-- ACTUALIZO UNA EMPRESA 
UPDATE empresas e
  SET e.fechaEntrada='2022/7/16'
  WHERE e.empresa='carrefour';

-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;


-- CREAR UN DISPARADOR PARA CLIENTES QUE CUANDO INSERTO UNO NUEVO
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2
-- ES NECESARIO PASAR EL CODIGO EN EL INSERT

DROP TRIGGER IF EXISTS biclientes;
DELIMITER //
CREATE TRIGGER biclientes
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  
  SET NEW.referencia=CONCAT(NEW.nombre,NEW.codigo);

END //
DELIMITER ;

-- introduzco un cliente
INSERT INTO clientes (codigo,nombre, empresa,fechaNacimiento)
  VALUES (100,'silvia', 'alpe', '2000-1-2');


-- compruebo
SELECT * FROM clientes c;

-- si queremos introducir variaos clientes lo tengo que realizar en varios insert
INSERT INTO clientes (codigo,nombre, empresa,fechaNacimiento)
  VALUES (110,'lorena', 'alpe', '2000-1-2'),(120,'luis', 'alpe', '2000-1-2');

-- compruebo
SELECT * FROM clientes c;


-- MODIFICAR EL DISPARADOR PARA CLIENTES QUE CUANDO INSERTO UNO NUEVO
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2
-- NO LE PASAMOS EL CODIGO EN EL INSERT
-- ATENCION : DIFICULTAD ALTA

DROP TRIGGER IF EXISTS biclientes;
DELIMITER //
CREATE TRIGGER biclientes
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  DECLARE numero int DEFAULT 0;

  -- esta solucion el problema es que calcula
  -- el ultimo id asignado en esta session
  -- no a la tabla clientes
  -- SET numero = LAST_INSERT_ID()+1; -- leo el proximo codigo 
  
  -- necesito hacer que la tabla de estadisticas se actualice
  -- sin necesidad de ejecutar el comando ANALIZE TABLE nombreTabla;
  set @copia=@@information_schema_stats_expiry; -- realizo una copia del valor de la variable del sistema
  SET @@information_schema_stats_expiry=0; -- obligo a que se actualicen las estadisticas automaticamente

-- en la variable numero tengo
-- el proximo id auto_increment a entregar en la tabla clientes
  SELECT 
    AUTO_INCREMENT
  INTO 
    numero
  FROM INFORMATION_SCHEMA.TABLES
    WHERE 
  table_name = 'clientes' AND TABLE_SCHEMA='disparadores1';
  
  SET NEW.referencia=CONCAT(NEW.nombre,numero);

  -- dejo la variable del sistema al valor original
  set @@information_schema_stats_expiry=@copia;

END //
DELIMITER ;

-- introduzco un cliente
INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('silvia', 'alpe', '2000-1-2');


-- compruebo
SELECT * FROM clientes c;

-- si queremos introducir variaos clientes lo tengo que realizar en varios insert
INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('lorena', 'alpe', '2000-1-2');

INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('luis', 'alpe', '2000-1-2');


-- CREAR UN DISPARADOR PARA CLIENTES QUE CUANDO ACTUALIZO UN CLIENTE
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2

DROP TRIGGER IF EXISTS buclientes;
DELIMITER //
CREATE TRIGGER buclientes
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  SET NEW.referencia=CONCAT(NEW.nombre,NEW.codigo);
END //
DELIMITER ;

-- actualizar un registro
UPDATE clientes c
  set c.nombre="jose"
  WHERE c.codigo=2;

SELECT * FROM clientes;

-- si por lo que sea queremos arreglar las referencias mal colocadas
-- voy a crear un procedimiento
-- que me actualice los registros que esten mal calculadas las referencias
DROP PROCEDURE IF EXISTS referencias;
DELIMITER //
CREATE PROCEDURE referencias()
BEGIN
  -- actualizo los registros de clientes cuya referencia este mal calculada
  UPDATE clientes c SET codigo=codigo WHERE referencia<>CONCAT(nombre,codigo);
END//
DELIMITER ;


-- introduzco dos registros en un mismo insert
INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('cesar', 'alpe', '2000-1-2'),('lolo','alpe','2001-2-3');

-- el segundo coloca mal la referencia
SELECT * FROM clientes;

-- llamo al procedimiento
CALL referencias();

SELECT * FROM clientes;


-- CREAR UN DISPARADOR PARA LA TABLA CLIENTES
-- QUE CUANDO INSERTO UN CLIENTE NUEVO
-- ME COLOQUE COMO EMAIL=[REFERENCIA]@[EMPRESA].COM

DROP TRIGGER IF EXISTS biclientes1;
DELIMITER //
CREATE TRIGGER biclientes1
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  SET NEW.email=CONCAT(NEW.referencia,"@",NEW.empresa,".com");
END //
DELIMITER ;


-- CREAR UN DISPARADOR PARA LA TABLA CLIENTES
-- QUE CUANDO ACTUALIZO UN CLIENTE 
-- ME COLOQUE COMO EMAIL=[REFERENCIA]@[EMPRESA].COM

DROP TRIGGER IF EXISTS buclientes1;
DELIMITER //
CREATE TRIGGER buclientes1
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  SET NEW.email=CONCAT(NEW.referencia,"@",NEW.empresa,".com");
END //
DELIMITER ;

-- compruebos los disparadores anteriores
INSERT into clientes (nombre, empresa, fechaNacimiento)
  VALUES ('loreto','alpe','1980-8-7');

UPDATE clientes c
  set nombre="Ramon"
  where c.codigo=1;

SELECT * FROM clientes c;


-- disparador para clientes
-- cuando creo un cliente nuevo
-- tengo que actualizar el campo fechaUltimo de la empresa
-- a la que pertenece ese cliente

DROP TRIGGER IF EXISTS aiclientes;
DELIMITER //
CREATE TRIGGER aiclientes
AFTER INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  -- SOLAMENTE ACTUALIZA EL CAMPO FECHAULTIMO
  -- DE LA EMPRESA PARA LA QUE TRABAJA EL CLIENTE
  -- INTRODUCIDO
  UPDATE empresas e
    SET e.fechaUltimo=NOW()
    WHERE e.empresa=NEW.empresa;
END //
DELIMITER ;

SELECT * FROM empresas e;

INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('susana','alpe','1998-1-5');

INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('cesar','carrefour','1998-1-5');

SELECT * FROM empresas e;









